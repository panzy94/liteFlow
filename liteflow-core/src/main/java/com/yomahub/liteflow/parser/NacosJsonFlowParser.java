package com.yomahub.liteflow.parser;

import cn.hutool.core.util.ReUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.nacos.api.NacosFactory;
import com.alibaba.nacos.api.PropertyKeyConst;
import com.alibaba.nacos.api.config.ConfigService;
import com.alibaba.nacos.client.config.listener.impl.PropertiesListener;
import com.yomahub.liteflow.exception.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class NacosJsonFlowParser extends JsonFlowParser {
    private static final Logger LOG = LoggerFactory.getLogger(NacosJsonFlowParser.class);

    private static final String NACOS_CONFIG_REGEX = "^https?://.*/nacos/v1/cs/configs\\?";

    public NacosJsonFlowParser() {
    }

    @Override
    public void parseMain(String path) throws Exception {
        Map<String, String> paramMap = new HashMap();
        String serverAddr = ReUtil.getGroup0("//.*?/", path).replace("/", "");
        String paramPath = ReUtil.replaceAll(path, NACOS_CONFIG_REGEX, "");
        String[] params = paramPath.split("&");
        for (String param : params) {
            String[] map = param.split("=");
            paramMap.put(map[0], map[1]);
        }
        Properties properties = new Properties();
        String dataId = paramMap.get("dataId");
        String group = paramMap.get("group");
        if (!StrUtil.isBlank(paramMap.get("tenant"))) {
            properties.put(PropertyKeyConst.NAMESPACE, paramMap.get("tenant"));
        } else properties.put(PropertyKeyConst.NAMESPACE, "");
        properties.put(PropertyKeyConst.SERVER_ADDR, serverAddr);

        ConfigService configService = NacosFactory.createConfigService(properties);

        String content = configService.getConfigAndSignListener(dataId, group, 500, new PropertiesListener() {
            @Override
            public void innerReceive(Properties properties) {
                LOG.info("stating load flow config...");
                String content;
                try {
                    content = configService.getConfig(dataId, group, 500);
                    parse(content);
                } catch (Exception e) {
                    throw new ParseException(e.getMessage());
                }
            }
        });
        if (StrUtil.isBlank(content)) {
            String error = MessageFormat.format("the node[{0}] value is empty", path);
            throw new ParseException(error);
        }
        parse(content);
    }
}
